/**
 * TheCitadelofRicks
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.thecitadelofricks.hsoparser

import org.jsoup.Jsoup
import org.mosad.thecitadelofricks.Course
import org.slf4j.LoggerFactory
import java.net.SocketTimeoutException

class CourseListParser {

    private var logger: org.slf4j.Logger = LoggerFactory.getLogger(CourseListParser::class.java)

    /**
     * return a list of all courses at courseListURL
     * @param courseListURL the url to the course list page
     * @return a ArrayList<Course> with all courses or null if the request was not successful
     */
    fun getCourseLinks(courseListURL: String): HashMap<String, Course>? {
        val courseLinkList = HashMap<String, Course>()
        try {
            val courseHTML = Jsoup.connect(courseListURL).get()

            courseHTML.select("ul.index-group").select("li.Class").select("a[href]").forEachIndexed { _, element ->
                courseLinkList[element.text()] = Course(
                    element.text(),
                    element.attr("href").replace("http", "https")
                )
            }
            logger.info("successfully retrieved course List")
        } catch (ex: SocketTimeoutException) {
            logger.warn("timeout from hs-offenburg.de, updating on next attempt!")
            return null
        } catch (gex: Exception) {
            logger.error("general CourseListParser error", gex)
            return null
        }

        return courseLinkList
    }
}