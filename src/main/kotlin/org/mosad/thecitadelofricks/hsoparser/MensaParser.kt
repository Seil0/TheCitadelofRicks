/**
 * TheCitadelofRicks
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.thecitadelofricks.hsoparser

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.parser.Parser
import org.mosad.thecitadelofricks.Meal
import org.mosad.thecitadelofricks.MensaWeek
import org.slf4j.LoggerFactory
import java.net.SocketTimeoutException

class MensaParser {
    private var logger: org.slf4j.Logger = LoggerFactory.getLogger(MensaParser::class.java)

    /**
     * returns the mensa's menu for a week
     * @param mensaMenuURL the url to a mensa menu (swfr)
     * @return the mensa's menu as MensaWeek found at menuURL or null if the request was not successful
     */
    fun getMensaMenu(mensaMenuURL: String): MensaWeek? {
        return try {
            val menuHTML = Jsoup.connect(mensaMenuURL).timeout(15000).get()
            parseMensaMenu(menuHTML)
        } catch (ex: SocketTimeoutException) {
            logger.warn("timeout from $mensaMenuURL, updating on next attempt!")
            null
        } catch (gex: Exception) {
            logger.error("general MensaParser error", gex)
            null
        }
    }

    /**
     * parse the mensa's menu from the html document
     * @param htmlDoc the html document containing the menu
     * @return the mensa's menu as MensaWeek
     */
    fun parseMensaMenu(htmlDoc: Document): MensaWeek? {
        val mealWeekList = MensaWeek()

        try {
            htmlDoc.select("#tabsWeekdaysMenu").select("div.menu-tagesplan")
                .forEachIndexed { dayIndex, day ->
                    val meals = mealWeekList.days[dayIndex].meals
                    val strDay = day.select("h3").text()

                    day.select("div.menu-tagesplan > div.grid").first()?.select("div.flex-col")?.forEachIndexed { _, meal ->
                        val heading = meal.select("h5").text()
                        val parts = ArrayList(meal.select("small.extra-text").html().split("<br>").map { Parser.unescapeEntities(it, true).trim() })
                        val additives = meal.select("small.zusatzsstoffe[x-show=showAllergenes]").text()
                        parts.removeIf { x -> x.isEmpty() || x.isBlank() }

                        meals.add(Meal(strDay, heading, parts, additives))
                    }

                    if (meals.isEmpty()) {
                        // Add an empty Meal entry to preserve the day information
                        meals.add(Meal(strDay, "", ArrayList(), ""))
                    }
                }
        } catch (pex: Exception) {
            logger.error("error while parsing the html file", pex)
            return null
        }

        return mealWeekList
    }

    /**
     * return the link of the next weeks menus
     * @param mensaMenuURL the current weeks menus link
     */
    fun getMenuLinkNextWeek(mensaMenuURL: String): String {
        val menuHTML = Jsoup.connect(mensaMenuURL).get()
        return "https://www.swfr.de" + menuHTML.select("div.section-mensa").select("a.next-week").attr("href")
    }

}