/**
 * TheCitadelofRicks
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.thecitadelofricks

import org.mosad.thecitadelofricks.controller.CacheController
import org.mosad.thecitadelofricks.controller.CacheController.Companion.courseList
import org.mosad.thecitadelofricks.controller.CacheController.Companion.getLesson
import org.mosad.thecitadelofricks.controller.CacheController.Companion.getLessonSubjectList
import org.mosad.thecitadelofricks.controller.CacheController.Companion.getRoomSchedule
import org.mosad.thecitadelofricks.controller.CacheController.Companion.getTimetable
import org.mosad.thecitadelofricks.controller.CacheController.Companion.mensaMenu
import org.mosad.thecitadelofricks.controller.CacheController.Companion.roomList
import org.mosad.thecitadelofricks.controller.StartupController
import org.mosad.thecitadelofricks.controller.StatusController.Companion.getStatus
import org.mosad.thecitadelofricks.controller.StatusController.Companion.updateCourseListRequests
import org.mosad.thecitadelofricks.controller.StatusController.Companion.updateMensaMenuRequests
import org.mosad.thecitadelofricks.controller.StatusController.Companion.updateRoomListRequests
import org.mosad.thecitadelofricks.controller.StatusController.Companion.updateRoomScheduleRequests
import org.mosad.thecitadelofricks.controller.StatusController.Companion.updateTimetableRequests
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList

@RestController
class APIController {

    private val logger: Logger = LoggerFactory.getLogger(APIController::class.java)

    companion object {
        const val apiVersion = "1.4.0"
        const val softwareVersion = "1.3.1"
        val startTime = System.currentTimeMillis() / 1000
    }

    init {
        StartupController()
        CacheController()
    }

    @RequestMapping("/courseList")
    fun courseList(): CoursesListRet {
        logger.info("courseList request at ${LocalDateTime.now()}!")
        updateCourseListRequests()

        return CoursesListRet(courseList.meta, ArrayList(courseList.courses.values))
    }

    @RequestMapping("/mensamenu")
    fun mensamenu(): MensaMenu {
        logger.info("mensamenu request at ${LocalDateTime.now()}!")
        updateMensaMenuRequests()
        return mensaMenu
    }

    @RequestMapping("/timetable")
    fun timetable(
        @RequestParam(value = "course", defaultValue = "AI4") courseName: String,
        @RequestParam(value = "week", defaultValue = "0") week: Int
    ): TimetableCourseWeek {
        logger.info("timetable request at ${LocalDateTime.now()}!")
        updateTimetableRequests(courseName)
        return getTimetable(courseName, week)
    }

    @RequestMapping("/subjectList")
    fun lessonSubjectList(
        @RequestParam(value = "course", defaultValue = "AI4") courseName: String,
        @RequestParam(value = "week", defaultValue = "0") week: Int
    ): HashSet<String> {
        logger.info("subjectList request at ${LocalDateTime.now()}!")
        updateTimetableRequests(courseName)
        return getLessonSubjectList(courseName, week)
    }

    @RequestMapping("/lessons")
    fun lesson(
        @RequestParam(value = "course", defaultValue = "AI4") courseName: String,
        @RequestParam(value = "subject", defaultValue = "Mathematik 4") lessonSubject: String,
        @RequestParam(value = "week", defaultValue = "0") week: Int
    ): ArrayList<LessonWithRoom> {
        logger.info("lesson request at ${LocalDateTime.now()}!")
        updateTimetableRequests(courseName)
        return getLesson(courseName, lessonSubject, week)
    }

    @RequestMapping("/roomList")
    fun roomList(): RoomsListRet {
        logger.info("roomList request at ${LocalDateTime.now()}!")
        updateRoomListRequests()
        return RoomsListRet(roomList.meta, ArrayList(roomList.rooms.values))
    }

    @RequestMapping("/roomSchedule")
    fun roomSchedule(
        @RequestParam(value = "room", defaultValue = "B040") roomName: String,
        @RequestParam(value = "week", defaultValue = "0") week: Int
    ): RoomScheduleWeekRet {
        logger.info("roomSchedule request at ${LocalDateTime.now()}!")
        updateRoomScheduleRequests(roomName)
        return getRoomSchedule(roomName, week)
    }

    @RequestMapping("/status")
    fun status(): Status {
        logger.info("status request at ${LocalDateTime.now()}!")
        return getStatus()
    }

    @RequestMapping("/health")
    fun health(): Int {
        logger.info("health request at ${LocalDateTime.now()}!")
        return 200
    }

    /**
     * Deprecated section
     */

    // TODO remove this with API version 2.0.0
    @Deprecated("courses is replaced by courseList", replaceWith = ReplaceWith("courseList()"))
    @RequestMapping("/courses")
    fun courses(): CoursesListRet {
        return courseList()
    }

    // TODO remove this with API version 2.0.0
    @Deprecated("the parameter courseName is deprecated please use course", ReplaceWith("timetable(courseName, week)"))
    @RequestMapping("/timetable", params= ["courseName", "week"])
    fun timetableDep(
        @RequestParam(value = "courseName", defaultValue = "AI4") courseName: String,
        @RequestParam(value = "week", defaultValue = "0") week: Int
    ): TimetableCourseWeek {
       return timetable(courseName, week)
    }

}