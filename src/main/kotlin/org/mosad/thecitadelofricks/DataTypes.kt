/**
 * TheCitadelofRicks
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.thecitadelofricks

import java.time.LocalDateTime
import java.util.*
import kotlin.collections.HashMap

// data classes for the course part
data class Course(val courseName: String, val courseLink: String)

data class CoursesMeta(val updateTime: Long = 0, val totalCourses: Int = 0)

data class CoursesList(val meta: CoursesMeta = CoursesMeta(), val courses: SortedMap<String, Course>)
data class CoursesListRet(val meta: CoursesMeta = CoursesMeta(), val courses: ArrayList<Course> = ArrayList())

// data classes for the Mensa part
data class Meal(val day: String, val heading: String, val parts: ArrayList<String>, val additives: String)

data class Meals(val meals: ArrayList<Meal>)

data class MensaWeek(val days: Array<Meals> = Array(7) { Meals(ArrayList()) })

data class MensaMeta(val updateTime: Long, val mensaName: String)

data class MensaMenu(val meta: MensaMeta, val currentWeek: MensaWeek, val nextWeek: MensaWeek)

// data classes for the timetable part

data class CalendarWeek(val week: Int, val year: Int)

data class Lesson(
    val lessonID: String,
    val lessonSubject: String,
    val lessonTeacher: String,
    val lessonRoom: String,
    val lessonRemark: String
)

data class TimetableDay(val timeslots: Array<ArrayList<Lesson>> = Array(6) { ArrayList<Lesson>() })

data class TimetableWeek(val days: Array<TimetableDay> = Array(6) { TimetableDay() })

data class TimetableCourseMeta(var updateTime: Long = 0, val courseName: String = "", val weekIndex: Int = 0, var weekNumberYear: Int = 0, val year: Int = 0, val link: String = "")

data class TimetableCourseWeek(val meta: TimetableCourseMeta = TimetableCourseMeta(), var timetable: TimetableWeek = TimetableWeek())


// data classes for the status part

data class Status(
    val time: LocalDateTime,
    val uptime: String,
    val apiVersion: String,
    val softwareVersion: String,
    val totalRequests: Int,
    val mensaMenuRequests: Int,
    val courseListRequests: Int,
    val timetableRequests: HashMap<String, Int>,
    val timetableListSize: Int,
    val coursesLastUpdate: Date,
    val mensaLastUpdate: Date,
    val hsoResponseCode: Int,
    val swfrResponseCode: Int
)