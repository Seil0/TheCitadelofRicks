/**
 * TheCitadelofRicks
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.thecitadelofricks.controller

import org.mosad.thecitadelofricks.APIController.Companion.apiVersion
import org.mosad.thecitadelofricks.APIController.Companion.softwareVersion
import org.mosad.thecitadelofricks.APIController.Companion.startTime
import org.mosad.thecitadelofricks.Status
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.net.HttpURLConnection
import java.net.URI
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.HashMap

class StatusController {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(StatusController::class.java)

        var totalRequests = 0
            private set
        var mensaMenuRequests = 0
            private set
        var courseListRequests = 0
            private set
        var timetableRequests = HashMap<String, Int>()
            private set
        var roomListRequests = 0
            private set
        var roomScheduleRequests = HashMap<String, Int>()
            private set

        /**
         * if a mensamenu/courseList/timetable is requested update the specific and total request count
         */
        fun updateMensaMenuRequests() {
            mensaMenuRequests++
            totalRequests++
        }

        fun updateCourseListRequests() {
            courseListRequests++
            totalRequests++
        }

        fun updateTimetableRequests(courseName: String) {
            timetableRequests[courseName] = (timetableRequests[courseName] ?: 0) + 1
            totalRequests++
        }

        fun updateRoomListRequests() {
            roomListRequests++
            totalRequests++
        }

        fun updateRoomScheduleRequests(roomName: String) {
            roomScheduleRequests[roomName] = (roomScheduleRequests[roomName] ?: 0) + 1
            totalRequests++
        }

        fun getStatus(): Status {
            val currentTime = System.currentTimeMillis() / 1000
            val minutes = (currentTime - startTime) % 3600 / 60
            val hours = (currentTime - startTime) % 86400 / 3600
            val days = (currentTime - startTime) / 86400

            var hsoCode = 999
            var swfrCode = 999

            try {
                val hsoURL = URI("https://www.hs-offenburg.de/").toURL()
                val swfrURL = URI("https://www.swfr.de/").toURL()

                var connection = hsoURL.openConnection() as HttpURLConnection
                connection.requestMethod = "HEAD"

                connection.connectTimeout = 15000
                hsoCode = connection.responseCode

                connection = swfrURL.openConnection() as HttpURLConnection
                connection.connectTimeout = 15000
                swfrCode = connection.responseCode
            } catch (e: Exception) {
                logger.error("Error while fetching url response codes!", e)
            }

            return Status(
                LocalDateTime.now(),
                "$days days, $hours:$minutes",
                apiVersion,
                softwareVersion,
                totalRequests,
                mensaMenuRequests,
                courseListRequests,
                timetableRequests,
                CacheController.timetableList.size,
                Date(CacheController.courseList.meta.updateTime * 1000),
                roomListRequests,
                roomScheduleRequests,
                CacheController.roomScheduleList.size,
                Date(CacheController.roomList.meta.updateTime * 1000),
                Date(CacheController.mensaMenu.meta.updateTime * 1000),
                hsoCode,
                swfrCode
            )
        }

    }
}