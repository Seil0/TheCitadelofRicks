/**
 * TheCitadelofRicks
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.thecitadelofricks.controller

import com.google.gson.Gson
import com.google.gson.JsonParser
import org.mosad.thecitadelofricks.TimetableCourseWeek
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.*
import java.util.*

class StartupController {

    private val logger: Logger = LoggerFactory.getLogger(CacheController::class.java)

    companion object {
        val userHome: String = System.getProperty("user.dir")
        val tcorHome = "$userHome/tcor"
        val dirTcorHome = File(tcorHome)
        val dirTcorCache = File("$tcorHome/cache")
        val fileConfig = File("$tcorHome/config.xml")

        var cachetAPIKey = "0"
        var cachetBaseURL = "https://status.mosad.xyz"
        var courseListURL = "https://www.hs-offenburg.de/studium/vorlesungsplaene/"
        val roomListURL = "https://www.hs-offenburg.de/die-hochschule/organisation/infos-services/raumbelegungen"
        var mensaMenuURL = "https://www.swfr.de/essen/mensen-cafes-speiseplaene/mensa-offenburg"
        var mensaName = "Offenburg"
    }

    init {
        // if the tcor directory doesn't exist, create it
        if (!dirTcorHome.exists()) {
            dirTcorHome.mkdir()
        }

        // if the cache directory doesn't exist, create it
        if (!dirTcorCache.exists()) {
            dirTcorCache.mkdir()
        }

        // check if the config file exist, if so load it
        if (fileConfig.exists()) {
            loadConfig()
        } else {
            createConfig()
        }

        // read cached timetable files, as they are not initially cached
        readCachedTimetables()
    }

    /**
     * load the config stored in the config.xml file
     */
    private fun loadConfig() = try {
        val properties = Properties()
        properties.loadFromXML(FileInputStream(fileConfig))

        try {
            cachetAPIKey = properties.getProperty("cachetAPIKey")
        } catch (_: Exception) {}

        try {
            cachetBaseURL = properties.getProperty("cachetBaseURL")
        } catch (_: Exception) {}

        try {
            mensaMenuURL = properties.getProperty("mensaMenuURL")
        } catch (_: Exception) {}

        try {
            mensaName = properties.getProperty("mensaName")
        } catch (_: Exception) {}

    } catch (ex: Exception) {
        logger.error("error while loading config", ex)
    }

    /**
     * create an initial config file
     */
    private fun createConfig() = try {
        val properties = Properties()

        properties.setProperty("cachetAPIKey", cachetAPIKey)
        properties.setProperty("cachetBaseURL", cachetBaseURL)
        properties.setProperty("mensaMenuURL", mensaMenuURL)
        properties.setProperty("mensaName", mensaName)

        val outputStream = FileOutputStream(fileConfig)
        properties.storeToXML(outputStream, "tcor configuration")
        outputStream.close()
    } catch (ex: Exception) {
        logger.error("error while creating config", ex)
    }

    /**
     * read all previously cached timetables
     */
    private fun readCachedTimetables() {
        dirTcorCache.walkTopDown().forEach {
            if (it.isFile && it.name.endsWith(".json")) {
                val fileReader = FileReader(it)
                val bufferedReader = BufferedReader(fileReader)

                try {
                    val timetableObject = JsonParser.parseString(bufferedReader.readLine()).asJsonObject
                    val timetable = Gson().fromJson(timetableObject, TimetableCourseWeek().javaClass)
                    CacheController.timetableList["${timetable.meta.courseName}-${timetable.meta.weekIndex}"] =
                        timetable
                } catch (ex: Exception) {
                    logger.error("error while reading cache", ex)
                } finally {
                    bufferedReader.close()
                    fileReader.close()
                }
            }
        }
    }
}
