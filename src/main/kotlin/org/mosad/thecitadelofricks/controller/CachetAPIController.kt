/**
 * TheCitadelofRicks
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.thecitadelofricks.controller

import org.mosad.thecitadelofricks.controller.StatusController.Companion.totalRequests
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URI

class CachetAPIController {

     companion object {

         private val logger: Logger = LoggerFactory.getLogger(CachetAPIController::class.java)

         private var oldTotalRequests = 0

         fun postTotalRequests() {
             try {
                 val url = URI("${StartupController.cachetBaseURL}/api/v1/metrics/1/points").toURL()
                 val jsonInputString = "{\"value\": ${totalRequests -oldTotalRequests}, \"timestamp\": \"${(System.currentTimeMillis() / 1000)}\"}"
                 oldTotalRequests = totalRequests

                 val con = url.openConnection() as HttpURLConnection
                 con.requestMethod = "POST"
                 con.setRequestProperty("Content-Type", "application/json; utf-8")
                 con.setRequestProperty("Accept", "application/json")
                 con.setRequestProperty("X-Cachet-Token", StartupController.cachetAPIKey)
                 con.doOutput = true

                 val os = con.outputStream
                 val input = jsonInputString.toByteArray(charset("utf-8"))
                 os.write(input, 0, input.size)

                 val br = BufferedReader(InputStreamReader(con.inputStream, "utf-8"))
                 val response = StringBuilder()
                 var responseLine: String?

                 while (br.readLine().also { responseLine = it } != null) {
                     response.append(responseLine!!.trim { it <= ' ' })
                 }
                 logger.info(response.toString())

             } catch (e1: IOException) {
                 e1.printStackTrace()
             }
         }

     }

}