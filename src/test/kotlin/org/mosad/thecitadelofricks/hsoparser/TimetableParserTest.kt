/**
 * TheCitadelofRicks
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.thecitadelofricks.hsoparser

import org.jsoup.Jsoup
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mosad.thecitadelofricks.CalendarWeek
import java.io.File

class TimetableParserTest {

    @Test
    fun parseTimetableNormalWeek() {
        val htmlFile = File(TimetableParserTest::class.java.getResource("/html/Timetable_normal-week.html").path)
        val htmlDoc = Jsoup.parse(htmlFile, "UTF-8", "https://www.hs-offenburg.de/")
        val actualTimetable = CourseTimetableParser(htmlDoc = htmlDoc).parseTimeTable().toString().trim()
        val expectedTimetable = TimetableParserTest::class.java.getResource("/expected/Timetable_normal-week.txt").readText().trim()

        Assertions.assertEquals(expectedTimetable, actualTimetable)
    }

    @Test
    fun parseTimetableEmptyWeek() {
        val htmlFile = File(TimetableParserTest::class.java.getResource("/html/Timetable_empty-week.html").path)
        val htmlDoc = Jsoup.parse(htmlFile, "UTF-8", "https://www.hs-offenburg.de/")
        val actualTimetable = CourseTimetableParser(htmlDoc = htmlDoc).parseTimeTable().toString().trim()
        val expectedTimetable = TimetableParserTest::class.java.getResource("/expected/Timetable_empty-week.txt").readText().trim()

        Assertions.assertEquals(expectedTimetable, actualTimetable)
    }

    @Test
    fun parseCalendarWeek() {
        val htmlFile = File(TimetableParserTest::class.java.getResource("/html/Timetable_normal-week.html").path)
        val htmlDoc = Jsoup.parse(htmlFile, "UTF-8", "https://www.hs-offenburg.de/")
        val actualCalendarWeek = CourseTimetableParser(htmlDoc = htmlDoc).parseCalendarWeek()

        Assertions.assertEquals(CalendarWeek(42, 2019), actualCalendarWeek)
    }
}