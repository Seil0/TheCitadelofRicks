/**
 * TheCitadelofRicks
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.thecitadelofricks.hsoparser

import org.jsoup.Jsoup
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.io.File

internal class MensaParserTest {
    private val mensaMenuURL = "https://www.swfr.de/essen/mensen-cafes-speiseplaene/mensa-offenburg"

    @Test
    fun parseMensaMenuNormalWeek() {
        val htmlFile = File(MensaParserTest::class.java.getResource("/html/Mensa_normal-week.html").path)
        val htmlDoc = Jsoup.parse(htmlFile,"UTF-8", "https://www.swfr.de/")
        val mensaWeek = MensaParser().parseMensaMenu(htmlDoc)
        val expectedOutput = MensaParserTest::class.java.getResource("/expected/Mensa_normal-week.txt").readText()

        Assertions.assertEquals(expectedOutput, mensaWeek.toString())
    }

    @Test
    fun parseMensaMenuEmptyWeek() {
        val htmlFile = File(MensaParserTest::class.java.getResource("/html/Mensa_empty-week.html").path)
        val htmlDoc = Jsoup.parse(htmlFile,"UTF-8", "https://www.swfr.de/")
        val mensaWeek = MensaParser().parseMensaMenu(htmlDoc)
        val expectedOutput = MensaParserTest::class.java.getResource("/expected/Mensa_empty-week.txt").readText()

        Assertions.assertEquals(expectedOutput, mensaWeek.toString())
    }

    // TODO add test for special days ie. public holiday

    @Test
    fun getMenuLinkNextWeek() {
        val urlNextWeek = MensaParser().getMenuLinkNextWeek(mensaMenuURL) // this need a connection to the swfr server
        Assertions.assertNotNull(urlNextWeek)
    }
}
