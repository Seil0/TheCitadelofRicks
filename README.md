![Website](https://img.shields.io/website?down_color=red&down_message=offline&label=tcor.mosad.xyz&up_color=brightgreen&up_message=online&url=https%3A%2F%2Ftcor.mosad.xyz%2Fhealth)
[![Build Status](https://ci.mosad.xyz/api/badges/Seil0/TheCitadelofRicks/status.svg)](https://ci.mosad.xyz/Seil0/TheCitadelofRicks)
[![Release](https://img.shields.io/badge/dynamic/json.svg?label=release&url=https://git.mosad.xyz/api/v1/repos/Seil0/TheCitadelofRicks/releases&query=$[0].tag_name)](https://git.mosad.xyz/Seil0/TheCitadelofRicks/releases)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
# TheCitadelofRicks

The API backend for [ProjectLaogai](https://git.mosad.xyz/Seil0/ProjectLaogai).
The documentation can be found in the [wiki](https://git.mosad.xyz/Seil0/TheCitadelofRicks/wiki).