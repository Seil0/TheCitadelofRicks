FROM eclipse-temurin:21-jre-alpine
RUN addgroup -S spring && adduser -S spring -G spring
#RUN groupadd -r spring && useradd -r -g spring spring # for openjdk:xx builds
RUN mkdir /tcor && chown spring:spring /tcor
USER spring:spring
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} thecitadelofricks.jar
ENTRYPOINT ["java","-Djavax.net.ssl.trustStore=/tcor/cacerts", "-Djavax.net.ssl.trustStorePassword=changeit", "-jar","/thecitadelofricks.jar"]
VOLUME /tcor
